﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

//IMPORTANT NOTES BEFORE USING THIS IPF:

//The functions provided herein have only been tested with KinSim v.4.21 in Igor Pro v8.
//One you have this ipf loaded, load the "kinSim_v4.21" ipf. 
//To begin, you will need to go to the top toolbar and under KinSim find "show KinSim panel".
//You will have to hit accept and then save your experiment.

//After that, go back to KinSim in the top toolbar and select "Load_case"
//Load the attached case

//The KinSim ipf MUST be edited in the following macro:
//"Mechanism_Check()"
//This macro produces a pop-up window that will force the user to hit enter
//after each iteration.
//To avoid this, comment out the following code (lines 828-830 in KinSim v.4.21)
//		if (ll==1)
//			doalert 0,"Mechanism has been changed."
//		endif
//This does not impact KinSim, it only disables the pop-up window.

//You must also load the 2 included waves (igor text files) and put them in root:

//Additional information is provided in the Supplemental Information in
//Schueneman et. al., ACS Earth and Space Chemistry, 2023/2024.

//********************************************************************************************

//Master function (which calls forth sub routines)
function runKS_iterCon_cases()

	Get_SF_AirNumDenAdj()	
	
	wave wUncorrected_VBS = root:VBS_Mass
	wave wSOA_measured = root:SOAConcentration_ugm3
	string VOCName = "Phenol"
	variable MW=94.11
	
	wave params = root:calc:paraminput
	params[6]=0
	
	//KinSim needs a molar VBS for internal calculations
	//KinSim assumes MW(OA)=250 g mol-1
	string DF= Getwavesdatafolder(wUncorrected_VBS,1)
	string nameog = nameofwave(wUncorrected_VBS)
	string newname = replacestring("_mass", nameOG, "_molar")
	duplicate/o wUncorrected_VBS, $DF+newName
	wave wVBSMol = $DF+newName
	wVBSmol=wUncorrected_VBS*(MW/250)
	changeVBSCoeffs(wVBSMol, VOCName)

	//the bulk of the work for this solver is done in prepRun_KSFitting
	PrepRun_KSfitting()
	setdatafolder root:Calc:
	wave newVBS, totSOA_ugm3_fit, Caltime, Scalingf_COA, newVBS_sigma
	duplicate/o newVBS, newVBS_mass
	duplicate/o newVBS_sigma, w_Sigma_mass
	
	//make Cstar waves for VBS plotting
	make/o/T/n=3 CStar_3bins = {"10", "100", "1000"}
	
	//start getting new [SOA] for different conditions
	duplicate/o totSOA_ugm3_fit, totSumAdded_SOA_ugm3_250gmol_VWLOn_PWLOn
	changeVBSCoeffs(newVBS, VOCName) // this function changes the coefficients in the mech. input.
  	execute "GetParametersInModel()"
	execute "mechanism_check()"	
	execute "GetParametersInModel()"
	execute "GetMechanismInModel()"
	runKS_VWLoff_PWLon()
	runKS_VWLoff_PWLoff()
	setdatafolder root:calc
	wave totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOn, totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOff
	
	//VBS fit from VWL on, PWL on SOA yield
	wave/Z temperature=root:calc:scalingf_t_k
	make/o/d/n=(numpnts(root:calc:caltime)) tempW
	if(!waveexists(root:Calc:ScalingF_T_K))
		tempW=params[2]
	else
		tempW=(temperature*(params[2]+273.15))-273.15
	endif
	setdatafolder root:calc:
	calcYieldFromVBS(newVBS); wave SOAy
	duplicate/o SOAy, SOAMolarYield_VWLOn_PWLon_fromFit
	
	//VBS fit from VWL on, PWL on SOA yield
	setdatafolder root:calc:
	newVBS_mass = newVBS * (250/MW)
	w_sigma_mass = newvbs_sigma //* (250/MW)
	calcYieldFromVBS(newVBS_mass); wave SOAy
	duplicate/o SOAy, SOAMassYield_VWLOn_PWLon_fromFit
	killWaves/Z SOAy, w_coef, w_coef1, w_sigma, w_Sigma1
	wave wFP_new=calcFpVBS(VOCName, newVBS_mass)
	duplicate/o wFp_new, wFp_new_p
	wFp_new_p=newVBS_mass*wFp_new
	duplicate/o wFp_new, wFp_new_g
	wFp_new_g = newVBS_mass*(1-wFp_new)
	
	//make graph
	displayKSResults(wUncorrected_VBS, wSOA_measured)
end


//****************************************************************************************

// Converts the # cm-3 output of KinSim to ug m-3 for the SOA.
function Molcc_ugm3_converter(Concentration_moleccc, v_gmol)
	variable Concentration_moleccc		// Output by KinSim in # cm-3
	variable v_gmol	// Average g/mol of molecule condensing to the particle phase.
		
	return Concentration_moleccc/(6.022e23) * v_gmol * 1e6 * 1e6
	
end

//****************************************************************************************

//Three terms to be fit: VBS at 3 C* bins (as described in the funcfit function fit3VBSs
//Sequence of running steps in "Notes_ToDos" notepad
//It says SOA, compare to PWL uncorrected SOA

function PrepRun_KSfitting()

	wave VBSMass = root:VBS_Mass
	wave SOA_measured = root:SOAConcentration_ugm3
	
	string VOCName = "Phenol"
	variable MW = 94.11
	
	string newdataFold = "root:funcfitCalcs"
	if(!datafolderexists(newdatafold))
		newdatafolder $newdatafold
	else
	endif
	
	setdatafolder $newDataFold
	
	make/o/T/n=3 T_Constraints
	T_Constraints[0] = {"K0 >= 0", "K1 >= 0", "K2 >= 0"}
	make/o/d/n=(numpnts(VBSMass)) New_FitCoefficients = VBSMass * (MW/250)
	wave Caltime = root:Calc:Caltime

	Duplicate/O New_FitCoefficients, epswave
	epswave = New_FitCoefficients/100

	make/o/n=1 iterNum = 1 //for keeping track of number of interations run
	variable starttime = datetime //for keeping track of total time takes to run
	FuncFit Fit3VBSs New_FitCoefficients SOA_measured /R /A=0 /X=Caltime /E=epswave /C=T_Constraints /F={0.95,4}
		
	print "Total Number of iterations: " + num2str(iterNum[0])
	variable endtime = datetime - starttime
	print "Computation time: " + num2str(endtime) + " s" //for keeping track of total time takes to run
	
	print "Final best fit: "
	print New_FitCoefficients
	
	duplicate/o new_fitcoefficients, newVBS
	wave w_sigma
	duplicate/o w_sigma, newVBS_sigma
	Print "Final volatility basis set: "
	print newVBS
	
End

//********************************************************************************************

//Function to IMPLICITLY fit three parameters: the VBS coeffs for C=10,100, and 1000. 
//The input parameters for the function are the three things this function will look for. 
//The first is the VBS coefficients, which you always need to input initial guesses
//The second is the measured SOA, which is regressed vs. the modeled SOA
//The way this is coded will allow for a minimization of the chi-squared between the modeled and
//measured SOA!

Function Fit3VBSs(w,totSOA_ugm3,timeWave) : FitFunc //w is the coefficient wave, i.e. the VBS coeffs for the 3 C* bins
//totSOA_ugm3 is the modeled KS [SOA]
//the timewave is the caltime
	Wave w, totSOA_ugm3, timeWave
	
	Variable V_FitMaxIters = 60
	wave StoicMat1 = root:calc:StoicMat1
	wave StoicMat2 = root:calc:StoicMat2
	wave StoicMat3 = root:calc:StoicMat3
	
	//commands from "Compile Mechanism" button. Needs to be run here since changing the A_Ainf values require mechanism_check()
	setdatafolder root:Calc:
	execute "GetParametersInModel()"
	execute "mechanism_check()"	

	//commands from "Make Model Ready to Run" button
	setdatafolder root:Calc:
	execute "GetParametersInModel()"
	execute "GetMechanismInModel()"
	execute "Conc_Initialization()"
	
	//These if/else statements need to be here. Despite the constraints, the model will choose stoich. coeffs.
	//<=0. Doing so removes those species from the mechanism in KinSim. 0.001 as a constraint adds almost no mass 
	//to that VBS bin.
	
	wave/T R1=root:calc:reactant1
	wave/T R2=root:calc:reactant2
	wave/T P1=root:calc:product1
	variable ii=0, jj=0
	
	//This function can technically be mused as-is for NO3 experiments (untested).
	for(ii=0; ii<numpnts(r1); ii++)
		if(stringmatch(R2[ii], "OH") || stringmatch(R2[ii], "NO3") )
			string VOCName = R1[ii]
			string OxName = R2[ii]
		else
		endif
	endfor	
	
	string VOCPref = VOCName[0,2]
	string C10 = VOCPref+Oxname+"_Cstar10"
	
	for(jj=0; jj<numpnts(r1);jj++)
		if(stringmatch(R1[jj], VOCName) && stringmatch(R2[jj], Oxname) && stringmatch(P1[jj], C10))
			variable Idx = jj
		endif
	endfor
	// && stringmatch(P1, pheOH_
	if(w[0]<=0)
		StoicMat1[Idx] = 0.001
	else
		StoicMat1[Idx] = w[0] //C*=10
	endif
	
	if(w[1]<=0)
		StoicMat2[Idx] = 0.001
	else
		StoicMat2[Idx] = w[1] //C*=100
	endif
	
	if(w[2]<=0)
		StoicMat3[Idx] = 0.001
	else
		StoicMat3[Idx] = w[2] //C*=1000
	endif
	
	//run the model
	Run_Model(Cont="restart", printhist="noprint")
	wave p_c10_moleccc = $"root:Calc:p_"+VOCPref+OXname+"_cstar10"
	wave p_c100_moleccc = $"root:Calc:p_"+VOCPref+Oxname+"_cstar100"
	wave p_c1000_moleccc = $"root:Calc:p_"+VOCPref+OxName+"_cstar1000"
	wave p_VOC = $"root:Calc:p_"+VOCName
	make/o/d/n=(numpnts(p_c10_moleccc)) totSOA_moleccc = p_VOC + p_c10_moleccc + p_c100_moleccc + p_c1000_moleccc
	
	//convert from molec/cc to ug/m3
	duplicate/o totSOA_moleccc, totSOA_ugm3
	totSOA_ugm3 = Molcc_ugm3_converter(totSOA_moleccc, 250)

	//copy waves for plotting output
	duplicate/o totSOA_ugm3, totSOA_ugm3_Fit
	duplicate/o Timewave, Caltime_Fit

	print w //so can monitor progress in command window

	//keep track of number of iterations (to print at end)
	wave iterNum = root:funcfitcalcs:iterNum
	iterNum += 1
	string tempW = "totSOA_ugm3_"+num2str(iternum[0])
	duplicate/o totSOA_ugm3, $tempW
	return 1
End

//********************************************************************************************

//function to change stoic. coeffs in kinsim mech
function changeVBSCoeffs(newVBS, VOCName)
	wave newVBS
	string VOCName
	VOCName  = "Phe"
	
	make/o/d/n=3 Cstar
	Cstar = {10,100,1000}
	
	setdatafolder root:calc //setdatafolder root:results
	//Read in SOA waves
	//SOA waves start with "p_". There are 3. 
	string SOAws = wavelist("p_*", ";", "")
	string notCstarWs = wavelist("!*cstar*", ";", "")
	string prevWs = wavelist("*prev*", ";", "")
	string contWs = wavelist("*cont*", ";", "")
	string VOCPref = VOCName[0,2]
	
	string oldVOC = wavelist("!*"+VOCPref+"*", ";", "")
	SOAws = removefromlist(contWs, SOAws)
	SOAws = removefromlist(prevws, SOAws)
	SOAws = removefromlist(notCstarWs, SOAws)
	SOAWs = removefromlist(oldVOC, SOAWs)
	variable NW = itemsinlist(SOAws)
	string gws=soaws
	gws = replacestring("p_", soaws, "")
	
	wave SC1 = root:Calc:StoicMat1
	wave SC2 = root:Calc:StoicMat2
	wave SC3 = root:Calc:StoicMat3
	wave/T R1 = root:Calc:Reactant1
	wave/T R2 = root:Calc:Reactant2
	wave/T P1 = root:Calc:Product1
	wave/T P2 = root:Calc:Product2
	wave/T P3 = root:Calc:Product3
	wave/T RxnUseWv = root:Calc:Disabled
	wave RateCoeff = root:Calc:A_Ainf
	
	variable numRxns = numpnts(R1)
	variable jj=0, kk=0
	make/o/d/n=(numRxns) keyWv_madeloop = 0
	
	for(jj=0; jj<numRxns; jj++)
		string nameOFR1 = R1[jj] 
		string nameofR2 = R2[jj]
		string inUse = RxnUseWv[jj]
		
		if((stringmatch(NameOfR1, VOCname) || stringmatch(NameofR2, VOCName)) && (stringmatch(inUse, "N")))
			string nameofP1 = P1[jj]
			string nameofP2 = P2[jj]
			string nameofP3 = P3[jj]
			
			for(kk=0; kk<nw; kk++)
				string sw_g = stringfromlist(kk, gws)
				if(stringmatch(nameofp1, sw_g) && (stringmatch(inUse, "N")))
 					if(stringmatch(nameofp1, "*10"))
						SC1[jj] = newVBS[0]
					elseif(stringmatch(nameofp1, "*100"))
						SC1[jj] = newVBS[1]
					elseif(stringmatch(nameofp1, "*1000"))
						SC1[jj] = newVBS[2]
					endif
					
				elseif(stringmatch(nameofp2, sw_g) && (stringmatch(inUse, "N")))
					if(stringmatch(nameofp2, "*10"))
						SC2[jj] = newVBS[0]
					elseif(stringmatch(nameofp2, "*100"))
						SC2[jj] = newVBS[1]
					elseif(stringmatch(nameofp2, "*1000"))
						SC2[jj] = newVBS[2]
					endif

				elseif(stringmatch(nameofp3, sw_g) && (stringmatch(inUse, "N")))
					if(stringmatch(nameofp3, "*10"))
						SC3[jj] = newVBS[0]
					elseif(stringmatch(nameofp3, "*100"))
						SC3[jj] = newVBS[1]
					elseif(stringmatch(nameofp3, "*1000"))
						SC3[jj] = newVBS[2]
					endif
									
				endif
			endfor 	
		else
		endif
	endfor

	setdatafolder root:calc
	wave product_1 = product1
	wave product_2 = product2
	wave product_3 = product3
	execute "GetMechanismInModel()"
	execute "mechanism_check()"
	execute "parameter_check()"
	execute "GetMechanismInModel()"
end


//********************************************************************************************

//Solve for the SOA yield using an input VBS
function calcYieldFromVBS(wVBS)
	wave wVBS
	wave params = root:calc:paraminput
	variable temperature_K= 298
	wave scalingt_K=root:calc:scalingf_T_k
	make/o/d/n=(numpnts(scalingt_K)) Temperature_C=(scalingT_K*(params[2]+273.15))-273.15

	wave wT = root:calc:caltime
	wave wOA = root:calc:scalingf_COA
	make/o/d/n=(numpnts(wt)) SOAy, C1_conc, C10_conc, C100_conc, C1000_conc
	
	calcCstar_tseries(10); wave Cstar_10_Tchg; duplicate/o Cstar_10_tchg, C10_conc
	calcCstar_tseries(100); wave Cstar_100_Tchg; duplicate/o Cstar_100_tchg, C100_conc
	calcCstar_tseries(1000); wave Cstar_1000_Tchg; duplicate/o Cstar_1000_tchg, C1000_conc
	
	if(numpnts(wVBS)==3)
		variable C10=wVBS[0]
		variable C100 = wVBS[1]
		variable C1000 = wVBS[2]
		SOAy = C10/(1+C10_conc/wOA) + C100/(1+C100_conc/wOA) + C1000/(1+C1000_conc/wOA)
	else
		variable C1=wVBS[0]
		C10 = wVBS[1]
		C100 = wVBS[2]
		C1000 = wVBS[3]
		SOAy = C1/(1+C1_conc/wOA) + C10/(1+C10_conc/wOA) + C100/(1+C100_conc/wOA) + C1000/(1+C1000_conc/wOA)
	endif
	
end

//********************************************************************************************

//function to run KinSim with VWL off/PWL off
function runKS_VWLoff_PWLoff()

	enableWalls("VWL_off")
	enablePartWalls("PWL_off")
	setdatafolder root:Calc:
	execute "GetParametersInModel()"
	execute "mechanism_check()"	
	execute "GetParametersInModel()"
	execute "GetMechanismInModel()"
	execute "Conc_Initialization()"
	
	//run the model
	Run_Model(Cont="restart", printhist="noprint")
	addPartWs()
	setdatafolder root:calc
	wave totSumAdded_SOA_ugm3_250gmol
	duplicate/o totSumAdded_SOA_ugm3_250gmol, totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOff
end

//********************************************************************************************

//function to run KinSim with VWL off/PWL on
function runKS_VWLoff_PWLon()

	enableWalls("VWL_off")
	enablePartWalls("PWL_on")
	setdatafolder root:Calc:
	execute "GetParametersInModel()"
	execute "mechanism_check()"	
	execute "GetParametersInModel()"
	execute "GetMechanismInModel()"
	execute "Conc_Initialization()"
	//run the model
	Run_Model(Cont="restart", printhist="noprint")
	addPartWs()
	setdatafolder root:calc
	wave totSumAdded_SOA_ugm3_250gmol
	duplicate/o totSumAdded_SOA_ugm3_250gmol, totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOn
end

//********************************************************************************************

//Calculating Fp
function/wave calcFpVBS(VOCName, VBS)
	string VOCname
	wave VBS
	
	string DF = Getwavesdatafolder(VBS,1)
	setdatafolder root:calc:
	wave wOA=root:calc:scalingf_coa //root:results:coa
	wavestats/Q wOA
	variable cOA_max = v_max
	variable pt_cOA_max = V_maxLoc
	string SOAws = wavelist("p_*", ";", "")
	string notCstarWs = wavelist("!*cstar*", ";", "")
	string prevWs = wavelist("*prev*", ";", "")
	string contWs = wavelist("*cont*", ";", "")
	string VOCPref = VOCName[0,2]
	string oldVOC = wavelist("!*"+VOCPref+"*", ";", "")
	SOAws = removefromlist(contWs, SOAws)
	SOAws = removefromlist(prevws, SOAws)
	SOAws = removefromlist(notCstarWs, SOAws)
	SOAWs = removefromlist(oldVOC, SOAWs)
	if(numpnts(VBS)==3)
		string cstar1 = wavelist("*1", ";", "")
		SOAWs = removefromlist(cStar1, SOAws)
		make/o/T CStars = {"10", "100", "1000"}
	else
		CStars = {"1", "10", "100", "1000"}
	endif
	string gWs = replacestring("p_", SOAws, "")

	variable NW = itemsinlist(SOAws)
	wave w1 = $stringfromlist(0, SOAws)
	variable ii
	
	make/o/d/n=(numpnts(VBS)) $DF+"wFp"/WAVE=wFp
	for(ii=0; ii<numpnts(VBS); ii++)
		wave pw = $stringfromlist(ii, SOAws)
		wave gw = $stringfromlist(ii, gWs)
		wFp[ii]=pw[pt_COA_max]/(pw[pt_COA_max]+gw[pt_COA_max])
	endfor
	setdatafolder $DF
	return wFP
end

//********************************************************************************************

//reading in wall stuff
function enableWalls(sKey)
	string sKey //sKey = "VWL_on" or "VWL_off"
	
	string VOCName = "Phenol"
	variable numCstarbin = 3
	
	wave/T RxnUseWv = root:Calc:Disabled
	wave/T R1 = root:Calc:Reactant1
	wave/T R2 = root:Calc:Reactant2
	wave/T P1 = root:Calc:Product1
	wave/T P2 = root:Calc:Product2
	wave/T P3 = root:Calc:Product3
	variable nRxns = numpnts(R1)
	
	setdatafolder "root:calc:"
	//here is using "w_" functionality
	string partWallWs = wavelist("*partWall", ";", "")
	if(numCstarbin==3)
		string cstar1 = wavelist("*star1_partWall", ";", "")
		partWallWs = removefromlist(cStar1, Partwallws)
	endif
	string VOCPref = VOCName[0,2]
	string SOAws1 = partWallws
	string SOAws = replacestring("_partWall", partWallWs, "")
	SOAWs = replacestring(""+VOCPref+"", SOAWs, "p_"+VOCPref) //for when using "_wall" at end
	string gasWallWs = replacestring("p_", SOAws, "w_")
	string gWs = replacestring("p_", SOAws, "")
	variable numRs = itemsinlist(gasWallWs)
	
	string yorN
	if(stringmatch(skey,"VWL_on"))
		yorN="N"
	elseif(stringmatch(skey, "VWL_off"))
		yorN = "Y"
	endif

	variable ii=0, jj=0	
	for(ii=0; ii<numpnts(R1); ii++)
		string locReac1 = R1[ii]
		string locProd1 = P1[ii]
		
		for(jj=0; jj<numRs; jj++)
			string sw_g = stringfromlist(jj, gWs)
			string sW_gW = stringfromlist(jj, gasWallWs)
			string sW_p = stringfromlist(jj, SOAws)
			string sW_pW = stringfromlist(jj, partWallWs)
			if(stringmatch(locReac1, sw_g) && stringmatch(locProd1, sw_gW) || (stringmatch(locReac1, sw_gW) && stringmatch(locProd1, sw_g)))
				RxnUsewv[ii]=YorN
			elseif(stringmatch(locReac1, sw_p) && stringmatch(locProd1, sw_pW))// || (stringmatch(locReac1, partWallWs[jj]/2) && stringmatch(locProd1, SOAWs[jj])))
				RxnUsewv[ii]="N"
			endif
		
		endfor
		
	endfor
	//execute "mechanism_check()"	
end

//********************************************************************************************

//reading in wall stuff
function enablePartWalls(sKey)
	string sKey //sKey = "VWL_on" or "VWL_off"
	
	variable numCstarbin = 3
	string VOCName = "Phenol"
	wave/T RxnUseWv = root:Calc:Disabled
	wave/T R1 = root:Calc:Reactant1
	wave/T R2 = root:Calc:Reactant2
	wave/T P1 = root:Calc:Product1
	wave/T P2 = root:Calc:Product2
	wave/T P3 = root:Calc:Product3
	variable nRxns = numpnts(R1)
	
	setdatafolder "root:calc:"
	string partWallWs = wavelist("*partWall", ";", "")
	if(numCstarbin==3)
		string cstar1 = wavelist("*star1_partWall", ";", "")
		partWallWs = removefromlist(cStar1, Partwallws)
	endif
	string VOCPref = VOCName[0,2]
	string startW = wavelist("w_*", ";", "")
	partWallWs = removefromlist(startW, partWallWs)
	string gasWallWs = replacestring("_partWall", partWallWs, "_wall")
	string VOCName_p = "p_"+VOCNAme
	string VOCName_partWall = VOCName + "_partWall"
	string SOAws1 = partWallws
	SOAws1 = removefromlist(VOCName_partWall, SOAWs1)
	SOAWs1 = replacestring(""+VOCPref+"", SOAWs1, "p_"+VOCPref) //for when using "_wall" at end
	string SOAws2 = replacestring("_partWall", SOAws1, "")
	string SOAws = VOCName_p+";"+SOAWs2
	string SOApws = VOCNAme_partWall+";"+SOAWs1
	
	string gWs = replacestring("p_", SOAws, "")
	variable numRs = itemsinlist(gasWallWs)
	
	string yorN
	if(stringmatch(skey,"PWL_on"))
		yorN="N"
	elseif(stringmatch(skey, "PWL_off"))
		yorN = "Y"
	endif

	variable ii=0, jj=0	
	for(ii=0; ii<numpnts(R1); ii++)
		string locReac1 = R1[ii]
		string locProd1 = P1[ii]
		
		for(jj=0; jj<numRs; jj++)
			string sw_g = stringfromlist(jj, gWs)
			string sW_gW = stringfromlist(jj, gasWallWs)
			string sW_p = stringfromlist(jj, SOAws)
			string sW_pW = stringfromlist(jj, partWallws)
			if(stringmatch(locReac1, sw_p) && stringmatch(locProd1, sw_pW))// || (stringmatch(locReac1, partWallWs[jj]/2) && stringmatch(locProd1, SOAWs[jj])))
				RxnUsewv[ii]=YorN
			endif
		
		endfor
		
	endfor
	//execute "mechanism_check()"
end

//********************************************************************************************

//Calculate the Cstar at 298K values in a system with nonconstant temperatures 
function calcCstar_tseries(Cstar_298)
	variable Cstar_298
	wave scalingT_K = root:calc:scalingf_T_K
	wave params=root:calc:paraminput
	make/o/d/n=(numpnts(root:calc:caltime)) Temperature_C=(ScalingT_K*(params[2]+273.15))-273.15
	make/o/d/n=(numpnts(temperature_C)) $"Cstar_"+num2str(Cstar_298)+"_TChg"/WAVE=Cstar_T
	Cstar_T=CStar_298 * (298 /(273.15+Temperature_C))  * exp(-1*dHvap_CappaJimenez(CStar_298)*(1/(Temperature_C+273.15)-1/298)/8.3145)
	
	return 1
end

//********************************************************************************************

//function to add all of the "p_" values together from KinSim
function addPartWs()

	setdatafolder root:calc
	string VOCName="Phenol"
	string SOAws = wavelist("p_*", ";", "")
	string notCstarWs = wavelist("!*cstar*", ";", "")
	string prevWs = wavelist("*prev*", ";", "")
	string contWs = wavelist("*cont*", ";", "")
	string VOCPref = VOCName[0,2]
	string oldVOC = wavelist("!*"+VOCPref+"*", ";", "")
	SOAws = removefromlist(contWs, SOAws)
	SOAws = removefromlist(prevws, SOAws)
	SOAws = removefromlist(notCstarWs, SOAws)
	SOAWs = removefromlist(oldVOC, SOAWs)
	
	variable NW = itemsinlist(SOAws)
	wave w1 = $stringfromlist(0, SOAws)
	variable ij
	make/o/d/n=(numpnts(w1)) totSum_SOA
	
	for(ij=0; ij<nw; ij++)
		string sW2 = stringfromlist(ij, SOAws)
		wave wT_VWL = $sW2
		concatenate {wT_VWL}, totSum_SOA
	endfor
	
	Sumdimension/D=1/DEST=totSumAdded_SOA totSum_SOA
	duplicate/o totsumADded_SOa, totSumAdded_SOA_ugm3_250gmol
	totSumAdded_SOA_ugm3_250gmol=totSumAdded_SOA*((1/6.022e23)*(250/1)*(1e6/1)*((100^3)/1))
	 
end

//********************************************************************************************

//function to make final output graph
function displayKSResults(wUncorrected_VBS, wSOA_measured)
	wave wUncorrected_VBS, wSOA_measured //unfitted VBS is the VBS fit using the measured SOA yield and OA (PWL corrected, VWL uncorrected)
	string VOCName = "Phe"
	killwindow/Z IterationResults //kill results window (can disable this feature if needed)
	setdatafolder root:calc:
	
	//read in waves
	wave calTime=root:calc:Caltime
	wave params = root:calc:paraminput
	wave wOA = root:calc:scaling_fCOA
	wave/Z temperature=root:calc:scalingf_t_k
	make/o/d/n=(numpnts(root:calc:caltime)) tempW
	if(!waveexists(root:Calc:ScalingF_T_K))
		tempW=params[2]
	else
		tempW=(temperature*(params[2]+273.15))-273.15
	endif
	
	//Calculate SOA yield for the uncorrected VBS	
	//VBS fit from VWL on, PWL on SOA yield
	calcYieldFromVBS(wUncorrected_VBS); wave SOAy
	duplicate/o SOAy, SOAMassYield_og
	
	string DF = Getwavesdatafolder(wUncorrected_VBS,1)
	string VBSog_sigma = DF+nameofwave(wUncorrected_VBS) + "_sigma"
	wave wVBSog_sigma = $VBSOg_sigma
	wave wFP_og=calcFpVBS(VOCName, wUncorrected_VBS)
	duplicate/o wFp_og, wFp_og_p
	wFp_og_p=wUncorrected_VBS*wFp_OG
	duplicate/o wFp_og, wFp_og_g
	wFp_og_g = wUncorrected_VBS*(1-wFp_OG)
	
	//VBS fit from VWL on, PWL on SOA yield
	setdatafolder root:calc:
	wave correctedVBS=newvbs_mass
	wave wFp_new_g, wFp_new_p, w_Sigma_mass, SOAMassYield_VWLOn_PWLon_fromFit
	make/o/T/n=3 CStar_T = {"10", "100", "1000"}

	
	enableWalls("VWL_ON")
	enablePartWalls("PWL_on")
	
	//read in waves from root:calc data folder
	wave wOA = scalingf_COA //input [OA], ug m-3
	wave wTime = Caltime //input time scale, one point every 15 seconds here
	string newSOAName = "totSumAdded_SOA_ugm3_250gmol_" //First half of SOA wave formed in runKS_iterCon_cases
	wave wSOA_VWLOn_PWLOn = $newSOAName+"VWLOn_PWLOn"
	wave wSOA_VWLOff_PWLOn = $newSOAName+"VWLOff_PWLOn"
	wave wSOA_VWLOff_PWLOff = $newSOAName+"VWLOff_PWLOff"
	
	//make window and display [SOA] 
	Display/N=IterationResults /W=(125.25,187.25,1085.25,557.75) wSOA_measured vs CalTime
	Appendtograph/W=IterationResults wSOA_VWLOn_PWLOn vs CalTime
	AppendToGraph/W=IterationResults wSOA_VWLOff_PWLOn vs CalTime
	AppendToGraph/W=IterationResults wSOA_VWLOff_PWLOff vs CalTime
	
	//Panel 2, SOA mass yields
	AppendToGraph/W=IterationResults/L=y1_SOAY/B=x1_SOAY SOAMassYield_og vs wOA
	AppendToGraph/W=IterationResults/L=y1_SOAY/B=x1_SOAY SOAMassYield_VWLOn_PWLOn_fromfit vs wOA

	//Panel 3, VBSs
	AppendToGraph/W=IterationResults/L=y2_VBS/B=x2_VBS wFp_og_g,wFp_og_p,wFp_new_g,wFp_new_p vs Cstar_T
	ModifyGraph rgb(wFp_og_g)=(65535,21845,0),rgb(wFp_og_p)=(65535,21845,0),rgb(wFp_new_g)=(1,26214,0)
	ModifyGraph rgb(wFp_new_p)=(1,26214,0)
	ModifyGraph hbFill(wFp_og_g)=8,hbFill(wFp_new_g)=6
	ModifyGraph toMode(wFp_og_g)=3,toMode(wFp_new_g)=3
	ErrorBars/T=2/L=2/RGB=(0,0,0) wFp_og_g Y,wave=(wVBSog_sigma,wVBSog_sigma)
	ErrorBars/T=2/L=2/RGB=(0,0,0) wFp_new_g Y,wave=(w_sigma_mass,w_sigma_mass)
	
	//Graph style (brute force coding for changing line colors/modes/etc).
	ModifyGraph lsize(totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOff)=2,lsize($nameofwave(wSOA_measured))=2,lsize(totSumAdded_SOA_ugm3_250gmol_VWLOn_PWLOn)=2,lsize(totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOn)=2
	ModifyGraph rgb(SOAMassYield_og)=(65535,21845,0),rgb(SOAMassYield_VWLOn_PWLOn_fromfit)=(1,26214,0)
	ModifyGraph rgb($nameofwave(wSOA_measured))=(65535,21845,0),rgb(totSumAdded_SOA_ugm3_250gmol_VWLOn_PWLOn)=(1,26214,0),rgb(totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOn)=(500, 500, 20000), rgb(totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOff)=(52428,52425,1)
	ModifyGraph lsize(SOAMassYield_og)=2,lsize(SOAMassYield_VWLOn_PWLon_fromFit)=2
	ModifyGraph fSize=15
	ModifyGraph lblPos(left)=84,lblPos(bottom)=48,lblPos(y1_SOAY)=62,lblPos(x1_SOAY)=47
	ModifyGraph lblPos(y2_VBS)=63,lblPos(x2_VBS)=38
	ModifyGraph lblLatPos(y1_SOAY)=3,lblLatPos(x1_SOAY)=-1,lblLatPos(y2_VBS)=-7,lblLatPos(x2_VBS)=2
	ModifyGraph freePos(y1_SOAY)={0,x1_SOAY}
	ModifyGraph freePos(x1_SOAY)={0,y1_SOAY}
	ModifyGraph freePos(y2_VBS)={0,x2_VBS}
	ModifyGraph freePos(x2_VBS)={0,y2_VBS}
	ModifyGraph axisEnab(bottom)={0,0.27}
	ModifyGraph axisEnab(x1_SOAY)={0.37,0.64}
	ModifyGraph axisEnab(x2_VBS)={0.74,1}
	Label left "[SOA] (ug m\\S-3\\M)"
	Label bottom "Experiment time (minutes)"
	Label y1_SOAY "SOA Mass yield"
	Label x1_SOAY "[OA] (µg m\\S-3\\M)"
	Label y2_VBS "VBS coefficient (mass)"
	Label x2_VBS "C\\S*\\M (T=298K)"
	SetAxis y1_SOAY 0,*
	SetAxis x1_SOAY 0,*
	SetAxis y2_VBS 0,1.3
	ShowInfo
	ModifyGraph muloffset($nameofwave(wSOA_measured))={0.0167,0},muloffset(totSumAdded_SOA_ugm3_250gmol_VWLOn_PWLOn)={0.0167,0},muloffset(totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOn)={0.0167,0},muloffset(totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOff)={0.0167,0}
	ModifyGraph/Z margin(top)=86
	Legend/C/N=text0/J/F=0/B=1/A=MC/X=-35.07/Y=68.54 "[SOA]:\r\\s("+nameofwave(wSOA_measured)+") Measured SOA"
	AppendText "\\s(totSumAdded_SOA_ugm3_250gmol_VWLOn_PWLOn) Modeled SOA (VWL on, PWL on)"
	AppendText "\\s(totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOn) Modeled SOA (VWL off, PWL on)"
	AppendText "\\s(totSumAdded_SOA_ugm3_250gmol_VWLOff_PWLOff) Modeled SOA (VWL off, PWL off)"
	Legend/C/N=text1/J/F=0/B=1/A=MC/X=2.20/Y=71.52 "Y\\BSOA\\M:\r\n\\s(SOAMassYield_og) Uncorrected\r\n\\s(SOAMassYield_VWLOn_PWLon_fromFit) Corrected for VWL"
	TextBox/C/N=text2/F=0/B=1/A=MC/X=37.71/Y=71.85 "VBS at C\\BOA\\M max:\r\n\\s(wFp_og_g) Original VBS (\\f02g\\f00)\\s(wFp_og_p)  (\\f02p\\f00)\r\\s(wFp_new_g) VWL corrected VBS (\\f02g\\f00)\\s(wFp_new_p) (\\f02p\\f00)"
	TextBox/C/N=text3/F=0/B=1/A=MC/X=-48.99/Y=47.19 "\\Z18A"
	TextBox/C/N=text3_0/F=0/B=1/A=MC/X=-11.63/Y=46.88 "\\Z18B"
	TextBox/C/N=text3_1/F=0/B=1/A=MC/X=25.55/Y=47.19 "\\Z18C"
end

//********************************************************************************************

Function dHvap_CappaJimenez(CStar298)
	//dHvap of Cappa and Jimenez 2010 ACP
	//enthalpy
	Variable CStar298
	Variable dH = (131-11*log(CStar298))*1000 // j/mol
	If(dH > 2e5)
		dH = 2e5
	EndIf
	Return dH
End
